- Feature Name: hunger_system
- Start Date: 2021-10-17
- RFC Number: (leave this empty for now - the core developers will give you a number to put here when the RFC is submitted)
- Tracking Issue: (leave this empty - the core developers will give you a number to put here when the RFC is submitted)

# Summary
[summary]: #summary

Add a hunger system whereby characters need to maintain supplies to keep full combat and working effectiveness

# Motivation
[motivation]: #motivation

There is a large disconnect between rtsim, where hunger limits population growth, and the in-game world, where it does not exist. This makes the large fields of crops and frequent dropping of foods by slaughtered creatures feel largely unjustified, and also disallows interesting world mechanics e.g. disrupting a cultist stronghold's supply lines until they surrender, or are weak enough to more easily take on.

# Guide-level explanation
[guide-level-explanation]: #guide-level-explanation

A hunger stat would be added which, when low, decreases a wide range of character stats, targeting an overall reduction of effectiveness of around 50% for most tasks, for example fighting, farming, and any other jobs we eventually get. In this document, I will use "satiation" to mean the opposite of "hunger."

Hunger would increase at a constant rate, taking five in-game days to go from entirely full to empty, with stat reduction beginning at 35% satiation and smoothly ramping up from nothing to the maximum impact, at which point it stabililises.

The specific debuffs while fully starved could include reducing maximum health and maximum stamina (-20%), weapon damage and movement speed (-30%), mining speed and collection speed (-50%), however the balance is difficult to judge without implemting.

Basic foods would automatically be consumed from a player's inventory as needed, in the order that they are placed in the inventory, as soon as hunger rises to the point that the next queued food would fully satiate the character (or perhaps a configurable lower point). There would be no requirement to sit or wait for food to be consumed, and these foods would give no buffs. The inventory would show a timer for how long current supplies will last. 

Complex meals would be crafted and consumed at bonfires, never held in the inventory, and would use basic foods as ingredients. These would provide medium length buffs, provide a little more satiation than their individual components, and able to increase satiation beyond 100% by a maximum of one meal. (For example, if you have 94% satiation, and eat a 30 satiation meal, you would have 124% satiation. Eating the same meal again would result in 130% satiation.)

Hunger would not increase while characters aren't actively played, but would save between sessions.

# Drawbacks
[drawbacks]: #drawbacks

Requires players to have some consideration of their food stocks, which may be considered an undue nuisance.

# Rationale and alternatives
[alternatives]: #alternatives

Most games with hunger mechanics will deal damage when a character's hunger meter is empty. This kind of micromanagement can be stressful and take away from other parts of the game. Since Veloren tends to be more action oriented than survival oriented (at least for the time being) it does not make too much sense to tie hunger to damage, and also doesn't seem necessary to meet the motivations of the system.

# Prior art
[prior-art]: #prior-art

Valheim uses a food system where food increases your maximum health. The previous section on alternatives explains why this mechanic is not the best fit for Veloren.
Minecraft has a hunger meter. When your hunger bar is empty, you take damage. How full or empty the hunger bar is does not matter provided it is not empty. This has the damage problem and even if we did adopt a similar system, it would not provide a separate utility for food from potions.
Dwarf fortress has hunger but I do not know the exact mechanics. I would assume increased hunger means less stamina, strength, and speed. Hunger also leads to death and as mentioned before, hunger causing death is a highly contentions topic in Veloren and intentionally not approached directly in this RFC.
Subnautica has separate hunger and thirst meters, each of which cause an increasing reduction in movement speed for each point below 20%, and kill the player character at 0%. Hunger follows similar recovery mechanics to those proposed here when foods would boost the meter above 100%. Food also spoils over time in that game, unless preserved with salt.

# Unresolved questions
[unresolved]: #unresolved-questions

- How would food spoilage (separate feature) play with this?
