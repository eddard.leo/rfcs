# Background:

Sandbox games have a lot of different methodology to presenting quests.  In a lot of such systems, questing breaks down to triggering a quest, performing an action, and receiving a reward.  This is very functional for scripted games that can present depth through storytelling and environments but in the core components is still simplistic in methodology but functional.  The difficulty in a procedural game is that depth in story telling  cannot be conveyed without direct programming.  The question becomes how to convey depth inside a procedural system.
    
There are procedural games that solve this issue with organic stories developed by a player’s actions.  Some of these games are Dwarf Fortress and Rimworld where when broken down, becomes a story of a player’s colony where the player’s reaction to the game world events.  Kenshi also presents an organic story where it creates a story based on how a player reacts to factions, the depth being that the factions themselves have depth to their desires, and a mimicry of differing social philosophy to which a player can either fight or join. The player's actions result in an organic interaction that develops into unique and memorable player experiences.
    
Depth can also be implemented in player choice with noticeable benefits and consequences, a prime example being Fallout: New Vegas where many quests will directly influence a player’s place in the world.  This depth is conveyed through noticable changes to environment and interactions with other factions, creating the illusion of a deep and alive game world.

# Goal:
    
This proposal is designed to try to convey depth by borrowing some of these systems to make an organic experience without direct storytelling.  This will look at the three main components of the questing cycle (triggering, action, reward)  and present core concepts and examples of how this may be implemented.

# Proposal Core Concepts and Implementation:

1. Add depth and variation to each step of questing components, being triggering, performing an action, and receiving an award.

    - Triggering: Finding an item, talking to an NPC, sneaking up and eavesdropping, visual marker while exploring, buying a person a drink at a bar, ambushed by a faction, looting/finding a map or note.

    - Action: Exploring a particular landscape, interrupting another faction’s action (ambush, ritual, raid), hunting a monster, crafting items, finding resource nodes, trading or smuggling with another faction, overthrowing another town, territory invasion, escorting/ambushing, exploring a trade route.

    - Reward: Experience, faction relation, items, crafting recipe, stat changes, training in weapon/armor, map information, rumor that may trigger another quest, trade information

2. Make the experience feel organic as though the player has stepped into a situation, not a floating exclamation mark, collect x number of boar tusks, get some magical pants.

    - Example of Organic Experience: Spot smoke plume in distance, scout it out and find it is a bandit camp, see that they are sitting around a fire, sneak up on them and eavesdrop that they are going to ambush a trade caravan.


3. Add a level of player choice to the “performing an action” and “reward” that may allow the player to take different physical actions opposed to just combat or exploration.

- Continuation of Organic Experience - Choices the Player Can Make:
    - Inform the locals of the bandit camp and help them raid the camp 
    - Inform another bandit camp of the ambush, result in warring at the ambush
    - Assist the bandits during the raid
    - Assist the traders
    - Attack the bandit camp
        - Ambush the caravan on your own
        - Offer to escort the caravan
    - Do nothing and watch the fireworks

4. Involve benefits and consequences to each action.  This can involve faction relations, environmental impacts, and player sacrifice (think TES Vampires).

- Continuation of Organic Experience, Repercussions of Player Actions: 
     - Inform the locals of the bandit camp and help them raid the camp, gain rep with town and gold, lose rep with bandits
    - Inform another bandit camp of the ambush, result in warring at the ambush, gain rep and items with other bandits, lose rep with other bandits
    - Assist the bandits during the raid, gain rep with bandits and trade goods, lose rep with traders
    - Assist the traders, gain gold and rep, lose rep with bandits
    - Attack the bandit camp gain items lose rep, gain rep with town
    - Ambush the caravan on your own, gain trade goods and lose rep
    - Offer to escort the caravan, gain rep and gold and learn trade information
- Other Repercussions outside of rep could be:
    - Hunting animals out of an area
    - Spread of undead, disease, corruption
    - Faction taking over resource node or town
    - Halted trade due to bandit activity
    - War between rival areas with increased patrols/spawn rate.




# Potential Way of Implementation:

Instead of a player “triggering” an event, have the event play out regardless based on a faction or group of NPC’s “Sphere of Influence”.  Right now, IIRC, towns have spheres of influence that take into consideration the surrounding environment to dictate their economy, size, and population.  Apply these same systems to different groups of NPC’s and have associative events occur within them.  If two spheres intersect, apply a conflict event between the two.  This could be rival factions fighting over resources, bandits attacking a trade sphere, druids and the undead battling, and other such examples.  The player will less “trigger” an event as they come across it organically.  The aforementioned ways of learning about the events can still be available, the choice of what action to take, and the result of the player interaction can still exist.  An additional layer of affecting a sphere of influence can be implemented, in the sense that if a player assists in a positive outcome, the sphere will grow.

Based on the faction documentation, if two factions have a good relationship, there will be no associative conflict events between the two but trade may develop which in itself could be an opportunity for trade associated quests.

Some examples of Spheres and their associative events

- Monster Sphere:
    - Magic (Cure disease, spread disease, assist in ritual)
    - Exploration (Hunt for resources, trap live species)
    - Social (Bait animals to capture or expand to show more prey animal, gain trust by feeding them for pets)
    - Combat (Kill alpha monsters, kill prey, kill rival predators, monsters raid villages and attack caravans)
- Faction and Town Spheres:
    - Trade (smuggle, actively trade).
    - Explore (Help find resources, find trade routes, sell map information, sell resources)
    - Magic (Cure disease, spread disease, offer magical services, assist in ritual)
    - Social (Sow discord, spread propaganda for/against, gain trust, dictate activities)
    - Craft (Setup contract crafting and fill orders)
    - Combat (Skirmishes, assassination/prevention of specific people, raid supplies, patrol roads, fist fight locals, attack/defend resource node, poison/prevent food/water supply)
- Examples of rival activities
    - Trade (smuggle).
    - Explore (sell fake map information)
    - Magic (spread disease/curse, interrupt/infiltrate ritual)
    - Social (Sow discord, spread propaganda against, gain trust, dictate activities)
    - Craft (Craft faulty equipment for locals)

