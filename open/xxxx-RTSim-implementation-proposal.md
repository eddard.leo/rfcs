# Simulating NPC Population Brainstorm and RTSim

In order to simulate a proper township, actual construction of a community or group must be considered.  A population requires specific resources to sustain itself, the group must work using the resources at hand, and specific parameters must be met in order for it to flourish.

Application of maslow’s hierarchy of needs can be applied in order to simulate a population fulfilling needs and growing and starting to influence the area around it.


The proposition is this: NPC’s will try to climb up the hierarchy of needs by interacting with the world and their surroundings. The stages will be slightly altered to promote gameplay.

Stage 1: Find the basic necessities (food, water, shelter, location)

Stage 2: Establish security (protect area and resources)

Stage 3: Increase population based on available resources

Stage 4: Acquire means to increase previous stages and increase populations

Stage 5: Spread Influence

# Anatomy of simulation and function

Each NPC population will have an area of influence and each population is trying to ascend the hierarchy of needs by going up to each stage.  Each population will be attributed a running score. Specific events will affect the score within the sphere of influence, such as trade, raids, defeating enemies, so on and so forth. Scores will also increase the size of influence.  To reach the next stage, the running score will need to reach a threshold.  In each stage, different means of gaining score will be unlocked with higher rate of returns for scores.  The score can also be decreased by interaction with other populations or player involvement, such as rival town burning crops, unsuccessful raid on a caravan, or external factors like sickness.


Each population has a predetermined hierarchy of progression which will affect how they interact with other populations.  This is the definition of how they mean to progress through the hierarchy of needs.  This can be industry, trade, conquest, diplomacy, and exploration.  Each population ranks the means of progression and can result in conflict if major differences occur.

Stage 1: A population will be spawned on the map with a predefined area of influence in which they interact with the world.  Within this area, the world will be sampled for presence of water, food, temperature, and natural resources.  If the parameters are met, a town is generated.  Within this generation are structures, farms and food stuffs, and the local population.  Scores are affected by gaining and losing resources such as growth or destruction of crops, hunting of animals, surviving weather events.

Stage 2: A population will establish a security force and start to secure the area of influence.  The population will try to regulate the local environment to increase safety, such as killing off predator animals, culling bandits, surviving raids, and establishing patrols.

Stage 3: A population will start to exploit local resources and make trade goods based on their hierarchy of progression.  Some industry will be established and perhaps a local government will be established (town officials, guilds, cross town companies).

Stage 4:  A population will establish means to gain more resources.  This goes back to their hierarchy of progression in order to make populations feel diverse.  A population may try to establish trade routes with like minded populations, or they may try for conquest, progress through industry, or maybe by finding natural resources.

Stage 5: A population will try to spread their influence as far as possible through the hierarchy of progression.  The end goal is to try to maximize the area of influence.

# Means to gain score per stage:

Stage 1: Gain food, building materials, hunt animals, build structures, kill predators

Stage 2: Kill predators, find resources, kill bandits, patrol an area

Stage 3: Make trade goods, find resources, establish trade routes, create stockpiles, protect traders

Stage 4: progression hierarchy takes into effect so attacking towns, trading with others, scouting a map, progressing technology and trade goods, diplomatic missions

Stage 5: All of the above

# Non-community based populations

This same system can be applied to animal populations but they may only go through stage 2.  It can also be applied to potential bandit camps, where they may survive and thrive because of their proximity to a trade route.


# Player Influence

Allow the player to contribute to a community’s score by helping or harming the community in their progression.  Immersion can be established by unique dialog options with the townsfolk where they will vocalize their wants and needs and lead hints into current activities they are partaking in that  a player can interact with.

# NPC Populations Interaction

If two populations' spheres of influences overlap, there will be interaction between them.  Based on the hierarchy of progression, the populations will respond differently.  If they have similar hierarchies, they will potentially share resources or at least trade, or combine forces.  If they do not, conflict will arise and world events may be spawned with simulated or real battles over resources or trade routes.

